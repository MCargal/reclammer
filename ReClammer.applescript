# ReClammer.applescript
# 
# Copyright (c) 2012 Misha Cargal
# 
# This software is provided 'as-is', without any express or implied
# warranty. In no event will the authors be held liable for any damages
# arising from the use of this software.
# 
# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:
# 
#    1. The origin of this software must not be misrepresented; you must not
#    claim that you wrote the original software. If you use this software
#    in a product, an acknowledgment in the product documentation would be
#    appreciated but is not required.
# 
#    2. Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software.
# 
#    3. This notice may not be removed or altered from any source
#    distribution.

on run
	set os_version to system version of (system info)
	
	# OS X 10.6.8 and earlier can Clamshell
	if os_version starts with "10.7" then
		modifyLCDBehavior()
	else if os_version starts with "10.8" then
		modifyLCDBehavior()
	else
		display dialog "This script is intended for OS X Lion and Mountain Lion"
	end if
end run

on modifyLCDBehavior()
	# the shell scripts
	set oldmode_script to "nvram boot-args=\"iog=0x0\""
	set newmode_script to "nvram -d boot-args"
	# the button strings
	set yes_button_text to "Modify"
	set undo_button_text to "Reset"
	set no_button_text to "Exit"
	set zap_pram_known_button_text to "Yes"
	set zap_pram_unknown_button_text to "No"
	# Apple Suport URL on PRAM & NVRAM zapping
	set zap_pram_url to "http://support.apple.com/kb/HT1379"
	# Message/Dialog Box strings
	set lcdmode_title to "Modify LCD Behavior"
	set lcdmode_mod_title to "LCD Behavior Modified"
	set lcdmode_reset_title to "LDC Behavior Reseted"
	
	set undo_message to "It's recomended that you zap the PRAM & NVRAM. Do you know how to?"
	set question_message to "Do you want disable the internal LCD so when screen is open and system is hooked-up to an external display as in Snow Leopard and earlier? 

An Administrator's credentials are going to be needed."
	set done_message to "Now your display should behave like it did in Snow Leopard and earlier. 

Just run this script again and select \"" & undo_button_text & "\" if you find the changes made unsuitable."
	
	try
		set lcdmode_question to display dialog question_message �
			with icon note �
			with title lcdmode_title �
			buttons {yes_button_text, undo_button_text, no_button_text} �
			default button 3 �
			cancel button 3
		set lcdmode_answer to button returned of lcdmode_question
		
		# Apply
		if lcdmode_answer is equal to yes_button_text then
			do shell script oldmode_script with administrator privileges
			display dialog done_message �
				with icon note �
				with title lcdmode_mod_title �
				buttons �
				"OK" default button 1
			requestReboot()
			# Remove
		else if lcdmode_answer is equal to undo_button_text then
			do shell script newmode_script with administrator privileges
			
			set zap_pram_question to display dialog undo_message �
				with title lcdmode_reset_title �
				with icon note �
				buttons {zap_pram_known_button_text, zap_pram_unknown_button_text} �
				default button 1
			set zap_pram_answer to button returned of zap_pram_question
			if zap_pram_answer is equal to zap_pram_unknown_button_text then
				open location zap_pram_url
			end if
			requestReboot()
		end if
		# Run away
	on error number -128
	end try
end modifyLCDBehavior

on requestReboot()
	set reboot_title to "Request to Restart"
	set reboot_message to "Would you like to restart now the system so changes can take effect?"
	try
		display dialog reboot_message �
			with title reboot_title �
			with icon note �
			buttons { "Yes", "No" } �
			default button 1 �
			cancel button 2
		if result = { button returned: "Yes" } then
			tell application "Finder" to restart
		end if
	on error number -128
	end try
end requestReboot